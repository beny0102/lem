﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ProyectoProductos
{
    class Producto
    {
        public Producto (string C, string D)
        {
            this.Codigo = C;
            this.Descripcion = D;
        }
        public string Codigo{get; set;}
        public string Descripcion{get; set;}
        public class PrecioFecha
        {
            DateTime FechaInicio;
            DateTime FechaFinal;
            Decimal Precio;
        }
        public List<PrecioFecha> Precios = new List<PrecioFecha>();
        //Atributo Lista de Precios
        public int Departamento{get; set;}
        public int Likes{get; set;}

    }
    class ProductoDB
    {
        public static void EscribeAtxt(string path, List<Producto> productos)
        {
            StreamWriter salidaTXT = new StreamWriter(
                                     new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write));
            foreach(Producto p in productos)
            {
                salidaTXT.WriteLine("{0}| {1}| ${2}", p.Codigo, p.Descripcion);
            }
            salidaTXT.Close();
        }
        public static List<Producto> LeerTXT(string path)
        {

        }
    }
    class PrecioFecha
    {
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Producto> productos = new List<Producto>();
            productos.Add(new Producto("#TELFJK123", "Teléfono Móvil de gama media Moto G8 Plus"));
            productos.Add(new Producto("#TELFUY152", "Teléfono Móvil de gama premium Samsumg Galaxy S9"));
            productos.Add(new Producto("#TELJGF785", "Teléfono Móvil de gama premium iPhone 11s"));
            productos.Add(new Producto("#TELKOP965", "Teléfono Móvil de gama media Redmi Note 8"));
            productos.Add(new Producto("#TELUJD425", "Teléfono Móvil de gama alta Huawei P30 Pro"));
            productos.Add(new Producto("#TELQWE753", "Teléfono Móvil de gama baja Moto E6 Power"));
            productos.Add(new Producto("#TELZXC159", "Teléfono Móvil de gama premium ASUS ROG Phone 2"));

            ProductoDB.EscribeAtxt(@"C:\Users\frank\Documents\Productos\Productos.txt", productos);



        }
    }
}
